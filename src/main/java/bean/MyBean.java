package bean;

import java.io.Serializable;

public class MyBean implements Serializable {
    private int id;
    private String name;
    public MyBean(){}

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
}
