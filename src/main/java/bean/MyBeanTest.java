package bean;

public class MyBeanTest {
    public static void main(String[] args) {
        MyBean myBean = new MyBean(); //object is created
        myBean.setName("Tarek"); //value is set
        System.out.println(myBean.getName());
    }
}
